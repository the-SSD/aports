# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=typst-lsp
pkgver=0.9.1
pkgrel=0
pkgdesc="Language server for typst"
url="https://github.com/nvarner/typst-lsp"
# typst, rust-analyzer
arch="aarch64 ppc64le x86_64"
license="MIT"
depends="rust-analyzer"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	"
source="
	https://github.com/nvarner/typst-lsp/archive/refs/tags/v$pkgver/typst-lsp-$pkgver.tar.gz
	"
options="net !check" # no tests

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build \
		--release \
		--frozen \
		--no-default-features \
		--features=remote-packages,native-tls
}

package() {
	install -Dm755 target/release/typst-lsp -t "$pkgdir"/usr/bin/
}

sha512sums="
8cf927c96af7ef1377babd5833512300f4dff8a7e81a556b039cfc92c4c031dae3161dbfbb224f05eacaf31d9c78f627c40eeab206be416833b31eb13d3d6667  typst-lsp-0.9.1.tar.gz
"
