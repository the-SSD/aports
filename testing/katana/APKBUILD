# Contributor: Sergiy Stupar <owner@sestolab.pp.ua>
# Maintainer: Sergiy Stupar <owner@sestolab.pp.ua>
pkgname=katana
pkgver=1.0.3
pkgrel=0
pkgdesc="Next-generation crawling and spidering framework"
url="https://github.com/projectdiscovery/katana"
arch="all"
license="MIT"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/projectdiscovery/katana/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\"" \
		-o "$pkgname" \
		-v "./cmd/$pkgname/main.go"
}

check() {
	go test -v ./...
}

package() {
	install -Dm755 "$pkgname" -t "$pkgdir"/usr/bin
}

sha512sums="
74876bdd881ce6a8123df87f112576e3aed9c7f563c5b31cd0e947c604de156ccb58124c4b8178a4651bf52e23f913afadfe188ebb58da0434e784757194e226  katana-1.0.3.tar.gz
"
