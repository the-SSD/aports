# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer:
pkgname=thin-provisioning-tools
pkgver=1.0.5
pkgrel=0
pkgdesc="suite of tools for manipulating the metadata of the dm-thin device-mapper target"
url="https://github.com/jthornber/thin-provisioning-tools"
# ucontext libc fs_type_t
arch="all !s390x"
license="GPL-3.0-or-later"
makedepends="cargo gawk cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jthornber/thin-provisioning-tools/archive/v$pkgver.tar.gz"
# tests do a ton of disk i/o for a while..
# they pass on x86_64, but a bit too destructive to constantly run
options="net !check"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --locked
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
1ba305770caf65fbba45ea4795bc7edf0dff06d72633f5293750051426c37a4ba44fffd71e2c57415af4683d8f3a8293b08eca0cdce45fd718b6699b7c419f2e  thin-provisioning-tools-1.0.5.tar.gz
"
