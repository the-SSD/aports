# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kwin
pkgver=5.27.7
pkgrel=1
pkgdesc="An easy to use, but flexible, composited Window Manager"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends="
	kirigami2
	qt5-qtmultimedia
	qt5-qtwayland
	xwayland
	"
depends_dev="
	breeze-dev
	eudev-dev
	fontconfig-dev
	kactivities-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdecoration-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kidletime-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kpackage-dev
	krunner-dev
	kscreenlocker-dev
	kservice-dev
	ktextwidgets-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	lcms2-dev
	libdrm-dev
	libepoxy-dev
	libinput-dev
	libqaccessibilityclient-dev
	libxcvt-dev
	libxi-dev
	libxkbcommon-dev
	mesa-dev
	mesa-gbm
	pipewire-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtscript-dev
	qt5-qtsensors-dev
	qt5-qtx11extras-dev
	wayland-dev
	xcb-util-cursor-dev
	xcb-util-image-dev
	xcb-util-wm-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	libcap-utils
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kwin.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kwin-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Broken

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/kwin.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	# significantly reduce debug symbol size
	# use None here to not make cmake pass bare -g (-g2)
	CFLAGS="$CFLAGS -O2 -g1" CXXFLAGS="$CXXFLAGS -O2 -g1" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd

	# kwin_wayland has CAP_SYS_NICE set. Because of this, libdbus doesn't trust the
	# environment and ignores it, causing for example keyboard shortcuts to not work
	# Remove CAP_SYS_NICE from kwin_wayland to make them work again
	setcap -r "$pkgdir"/usr/bin/kwin_wayland
}
sha512sums="
0f9f24d6296fdf7f7ab143ef38f69fb34aa0abbb39a24edebf4a59c1730ce5ccfebebbc1838c2e8eb94db11f8a7c30b6d4dfe2fd1848b9d938781256f555bf7e  kwin-5.27.7.tar.xz
"
