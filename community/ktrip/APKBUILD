# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktrip
pkgver=23.04.3
pkgrel=1
pkgdesc="A public transport assistant"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by qqc2-desktop-style
arch="all !armhf !s390x !riscv64"
url="https://invent.kde.org/utilities/ktrip"
license="GPL-2.0-only OR GPL-3.0-only"
depends="
	kde-icons
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcontacts-dev
	ki18n-dev
	kitemmodels-dev
	kpublictransport-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/ktrip.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktrip-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c340ba37871b975197d2a9938e18dda01571e1e6cf8e8964ff2c6bfdfff37be9a868dc61c80441d85660af10b19dd9171a4dbbd8f7fc6e9a335d5a3c903d3e08  ktrip-23.04.3.tar.xz
"
