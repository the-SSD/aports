# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=knights
pkgver=23.04.3
pkgrel=1
pkgdesc="Chess board by KDE with XBoard protocol support"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/games/knights/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kio-dev
	kplotting-dev
	ktextwidgets-dev
	kwallet-dev
	kxmlgui-dev
	libkdegames-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/knights.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/knights-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3d8babc35acf40c37e83c736377c486904951c57d259a8d4836a0da9dc808bca9be5088eb8de892cb7182d5d3612c7a77890844894997e1bcba22a6fb819534c  knights-23.04.3.tar.xz
"
