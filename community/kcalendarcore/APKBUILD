# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kcalendarcore
pkgver=5.108.0
pkgrel=2
pkgdesc="The KDE calendar access library"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="
	libical-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc"
_repo_url="https://invent.kde.org/frameworks/kcalendarcore.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcalendarcore-$pkgver.tar.xz"
options="!check" # RecursOn-ConnectDaily(2|3|6) make the builders stuck

replaces="kcalcore"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kcalendarcore.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# testrecurtodo, testreadrecurrenceid, testicaltimezones, testmemorycalendar and testtimesininterval are broken
	ctest --test-dir build --output-on-failure -E "test(recurtodo|readrecurrenceid|icaltimezones|memorycalendar|timesininterval)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
babca9e44582817ccf233f80bd519a91667374d5ab29918ac001b06b08568712f4e0b1f83aa28d7ff82f1e6609581ebbba1df051678389ff7da6e10881d72a83  kcalendarcore-5.108.0.tar.xz
"
