# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=exoscale
pkgver=1.71.2
pkgrel=1
pkgdesc="Command-line tool for everything at Exoscale"
url="https://github.com/exoscale/cli"
arch="all"
license="Apache-2.0"
makedepends="go"
source="https://github.com/exoscale/cli/archive/refs/tags/v$pkgver/exoscale-$pkgver.tar.gz"
builddir="$srcdir/cli-$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o exo -ldflags "-X main.version=$pkgver"
}

check() {
	go test github.com/exoscale/cli/cmd
}

package() {
	install -Dm755 exo -t "$pkgdir"/usr/bin
}

sha512sums="
58ed35125e7b0d86b43370f5361031a2229031766d5960b8620cd36314b006952ac3ca1f33e9548d0fdd6fbf5bc1a3ae3291ef712ad610bf4eb7e32273d9bc10  exoscale-1.71.2.tar.gz
"
