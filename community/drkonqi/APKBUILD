# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=drkonqi
pkgver=5.27.7
pkgrel=1
pkgdesc="The KDE crash handler"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="
	kirigami2
	kitemmodels
	"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	kuserfeedback-dev
	kwallet-dev
	kwidgetsaddons-dev
	kxmlrpcclient-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	syntax-highlighting-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/drkonqi.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/drkonqi-$pkgver.tar.xz"
subpackages="$pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/drkonqi.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=ON
	cmake --build build
}

check() {
	# connectiontest requires a network connection
	ctest --test-dir build --output-on-failure -E "connectiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7e600d6fff3af44d2d47cfdaec6edb99849e24f677b495cb28b55cd5d3d7803966dead8cfff11d94930f141dcaf5ea5022c64dfeba1fe03a316f7b2fe0e9e3ed  drkonqi-5.27.7.tar.xz
"
