# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ffmpegthumbs
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# not useful on s390x
arch="all !s390x !armhf"
url="https://www.kde.org/applications/multimedia/"
pkgdesc="FFmpeg-based thumbnail creator for video files"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ffmpeg-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	samurai
	taglib-dev
	"
_repo_url="https://invent.kde.org/multimedia/ffmpegthumbs.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ffmpegthumbs-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9b1d78c1ae8f8338abcb0afdfad3b25419a8c39ee4722251bd12db022c681b5bacdbdedc351fb4405e518177f15f9497182b6de1cc9f4f80793d6bfc3991c08f  ffmpegthumbs-23.04.3.tar.xz
"
